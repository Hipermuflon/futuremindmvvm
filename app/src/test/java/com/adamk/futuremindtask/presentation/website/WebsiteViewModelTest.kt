package com.adamk.futuremindtask.presentation.website

import com.adamk.futuremindtask.common.FutureViewModelTest
import com.jraska.livedata.test
import org.junit.Test

class WebsiteViewModelTest : FutureViewModelTest() {

    override val viewModel = WebsiteViewModel()
    private val testUrl = "www.google.pl"

    @Test
    fun shouldShowWebsiteOnRequest() {

        viewModel.run{
            requestUrl(testUrl)

            webData.test()
                .assertHasValue()
                .assertValue { it.url == testUrl}

            error.test()
                .assertNoValue()
        }
    }
}
