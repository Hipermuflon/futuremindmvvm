package com.adamk.futuremindtask.presentation.orders

import com.adamk.futuremindtask.common.FutureViewModelTest
import com.adamk.futuremindtask.domain.model.HttpError
import com.jraska.livedata.test
import com.nhaarman.mockito_kotlin.doNothing
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Test

class OrderViewModelTest : FutureViewModelTest(){

    private val model: OrderModel = mock()
    override val viewModel = OrderViewModel(model)
    private val testApiModel = OrderTestFactory.createOrderApiModel()
    private val expectedOrderList = OrderTestFactory.createOrderItems()

    @Test
    fun shouldPostOrdersOnBind() {

        whenever(model.downloadOrders()).thenReturn(Single.just(testApiModel))
        doNothing().whenever(model).saveOrders(expectedOrderList)

        viewModel.run{
            onBind()

            orders.test()
                .assertHasValue()
                .assertValue { it == expectedOrderList}

            error.test()
                .assertNoValue()

            isRefreshing.test()
                .assertHasValue()
                .assertValue { it == false}
        }
    }

    @Test
    fun shouldPostUnauthorizedErrorWhenThrown() {

        whenever(model.downloadOrders()).thenReturn(Single.error(Exception("401")))
        doNothing().whenever(model).saveOrders(expectedOrderList)

        viewModel.run{
            onBind()

            orders.test()
                .assertNoValue()

            error.test()
                .assertHasValue()
                .assertValue { it == HttpError.UNAUTHORIZED_ERROR}
        }
    }

    @Test
    fun shouldPostNotFoundErrorWhenThrown() {

        whenever(model.downloadOrders()).thenReturn(Single.error(Exception("404")))
        doNothing().whenever(model).saveOrders(expectedOrderList)

        viewModel.run{
            onBind()

            orders.test()
                .assertNoValue()

            error.test()
                .assertHasValue()
                .assertValue { it == HttpError.NOT_FOUND_ERROR}
        }
    }

    @Test
    fun shouldPostConnectionErrorWhenThrown() {

        whenever(model.downloadOrders()).thenReturn(Single.error(Exception("Unable to resolve host")))
        doNothing().whenever(model).saveOrders(expectedOrderList)

        viewModel.run{
            onBind()

            orders.test()
                .assertNoValue()

            error.test()
                .assertHasValue()
                .assertValue { it == HttpError.CONNECTION_ERROR}
        }
    }

    @Test
    fun shouldPostTimeoutErrorWhenThrown() {

        whenever(model.downloadOrders()).thenReturn(Single.error(Exception("TIME OUT")))
        doNothing().whenever(model).saveOrders(expectedOrderList)

        viewModel.run{
            onBind()

            orders.test()
                .assertNoValue()

            error.test()
                .assertHasValue()
                .assertValue { it == HttpError.TIMEOUT_ERROR}
        }
    }

    @Test
    fun shouldPostUnknownErrorWhenThrown() {

        whenever(model.downloadOrders()).thenReturn(Single.error(Exception("very unexpected things have happened")))
        doNothing().whenever(model).saveOrders(expectedOrderList)

        viewModel.run{
            onBind()

            orders.test()
                .assertNoValue()

            error.test()
                .assertHasValue()
                .assertValue { it == HttpError.UNKNOWN_ERROR}
        }
    }
}
