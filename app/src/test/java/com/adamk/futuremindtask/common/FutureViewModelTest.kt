package com.adamk.futuremindtask.common

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.adamk.futuremindtask.common.arch.FutureViewModel
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule

abstract class FutureViewModelTest {

    @Before
    fun mockRxSchedulers() = RxAndroidPlugins.setInitMainThreadSchedulerHandler{ Schedulers.trampoline()}

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    abstract val viewModel : FutureViewModel
}