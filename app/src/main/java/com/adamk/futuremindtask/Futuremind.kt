package com.adamk.futuremindtask

import android.app.Application
import com.adamk.futuremindtask.di.getModules
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.android.ext.android.startKoin

class Futuremind : Application() {

    override fun onCreate() {
        super.onCreate()
        initRealm()
        startKoin(this, getModules())
    }

    private fun initRealm() {
        Realm.init(this)
        Realm.setDefaultConfiguration(
            RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .build()
        )
    }
}