package com.adamk.futuremindtask.repository.database

import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class OrderRealm() : RealmObject() {

    @PrimaryKey
    var orderId: Int = 0
    var title: String = ""
    var description: String = ""
    var modificationDate: String = ""
    var imageUrl: String = ""
    var websiteUrl: String = ""

    constructor(orderItem: OrderItem) : this() {
        orderItem.let {
            title = it.title
            description = it.description
            orderId = it.orderId
            modificationDate = it.modificationDate
            imageUrl = it.imageUrl
            websiteUrl = it.websiteUrl
        }
    }
}
