package com.adamk.futuremindtask.repository.network

import com.adamk.futuremindtask.domain.client.ClientProvider
import com.adamk.futuremindtask.domain.services.DataService

class DataServiceImpl : DataService {

    override fun getData() = ClientProvider.get().getData()
}
