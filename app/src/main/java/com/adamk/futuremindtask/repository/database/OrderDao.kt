package com.adamk.futuremindtask.repository.database

import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem
import io.reactivex.Single
import io.realm.Realm

class OrderDao{

    fun saveData(orderItems: List<OrderItem>) = with(Realm.getDefaultInstance()){
        try {
            executeTransaction { realm ->
                realm.deleteAll()
                orderItems.forEach { item ->
                    realm.copyToRealmOrUpdate(OrderRealm(item))
                }
            }
        }
        catch (e: Exception) {
            cancelTransaction()
        }
    }

    fun getData() : Single<List<OrderRealm>>  = Single.just(
        Realm.getDefaultInstance()
            .where(OrderRealm::class.java)
            .findAll()
            .sort("orderId")
            .toList()
    )
}
