package com.adamk.futuremindtask.presentation.website

import android.os.Bundle
import com.adamk.futuremindtask.R
import com.adamk.futuremindtask.common.ui.FutureFragment
import com.adamk.futuremindtask.common.utils.SnackbarFactory
import com.adamk.futuremindtask.domain.model.HttpError
import kotlinx.android.synthetic.main.website_view.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class WebsiteFragment : FutureFragment(), WebsiteContract.View  {

    override val layoutResId = R.layout.website_view
    override val viewModel :  WebsiteViewModel by viewModel()

    override fun onViewsBound() {
        arguments?.getString(URLKEY)?.let{ url ->
            viewModel.requestUrl(url)
        }
        root.webViewRefresh.setOnRefreshListener(viewModel::requestUrl)
        viewModel.webData.observe(::getLifecycle, ::loadUrl)
        viewModel.isRefreshing.observe(::getLifecycle, ::setRefreshing)
        viewModel.error.observe(::getLifecycle, ::displayError)
    }

    override fun loadUrl(webData: WebData?) {
        webData?.let(root.webView::loadUrl)
    }

    override fun setRefreshing(isRefreshing: Boolean?) {
        root.webViewRefresh.isRefreshing = isRefreshing ?: false
    }

    override fun displayError(error: HttpError?) {
        error?.let{SnackbarFactory.showError(root, it)}
    }

    companion object {
        private const val URLKEY = "URL"

        fun newInstance(websiteUrl:String) = WebsiteFragment().also {fragment ->
            fragment.arguments = Bundle().also {it.putString(URLKEY,websiteUrl)}
        }
    }
}
