package com.adamk.futuremindtask.presentation.website

import android.arch.lifecycle.LiveData
import com.adamk.futuremindtask.common.arch.RefreshableViewModel
import com.adamk.futuremindtask.common.ui.views.ErrorView
import com.adamk.futuremindtask.common.ui.views.RefreshableView

class WebsiteContract {

    interface View : RefreshableView,ErrorView {
        fun loadUrl(webData: WebData?)
    }

    interface ViewModel : RefreshableViewModel{
        fun requestUrl(url: String)
        val webData : LiveData<WebData>
    }
}