package com.adamk.futuremindtask.presentation.website

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.graphics.Bitmap
import android.net.http.SslError
import android.os.Build
import android.webkit.*
import com.adamk.futuremindtask.common.arch.ErrorViewModelImpl
import com.adamk.futuremindtask.domain.model.HttpError

class WebsiteViewModel : ErrorViewModelImpl(), WebsiteContract.ViewModel {

    override fun onBind() = Unit

    override val webData = MutableLiveData<WebData>()
    override val isRefreshing= MutableLiveData<Boolean>()
    private var url: String? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun requestUrl(url: String) {
        this.url = url
        webData.postValue(
            WebData(
                createWebViewClient(),
                url
            )
        )
    }

    fun requestUrl(){
        url?.let{requestUrl(it)}
    }

    private fun createWebViewClient(): WebViewClient = object : WebViewClient() {

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            isRefreshing.postValue(true)
        }

        override fun shouldOverrideUrlLoading(webView: WebView, url: String) =  false

        override fun onReceivedError(webView: WebView?, request: WebResourceRequest?, error: WebResourceError) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                postError(error.errorCode.toAppError())
            }
        }

        override fun onReceivedSslError(webView: WebView?, handler: SslErrorHandler?, error: SslError?) {
            postError(HttpError.UNAUTHORIZED_ERROR)
        }

        override fun onReceivedError(webView: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
            postError(errorCode.toAppError())
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            isRefreshing.postValue(false)
        }
    }
}
