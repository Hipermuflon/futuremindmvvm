package com.adamk.futuremindtask.presentation.website

import android.webkit.WebViewClient

data class WebData(
    val webViewClient: WebViewClient,
    val url: String
)