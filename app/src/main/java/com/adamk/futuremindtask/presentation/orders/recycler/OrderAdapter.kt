package com.adamk.futuremindtask.presentation.orders.recycler

import android.view.ViewGroup
import com.adamk.futuremindtask.R
import com.adamk.futuremindtask.common.ui.recycler.FutureAdapter
import com.adamk.futuremindtask.databinding.OrderItemBinding

class OrderAdapter(
    val orders: List<OrderItem> = emptyList(),
    val onOrderClick: (webSite:String) -> Unit = {}
) : FutureAdapter<OrderItem>(orders) {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): FutureViewHolder<OrderItem> {
        return if(orders.isEmpty())
            EmptyViewHolder(inflate(R.layout.empty_view,parent))
        else
            OrderViewHolder(inflateBinding(R.layout.order_item,parent))
    }

    override fun getItemCount() = when (orders.size) {
        0 -> 1
        else -> super.getItemCount()
    }

    inner class OrderViewHolder(private val binding: OrderItemBinding) : FutureViewHolder<OrderItem>(binding.root) {

        override fun bind(orderItem: OrderItem) {
            itemView.apply {
                setOnClickListener {onOrderClick(orderItem.websiteUrl) }
                binding.orderItem = orderItem
            }
        }
    }
}
