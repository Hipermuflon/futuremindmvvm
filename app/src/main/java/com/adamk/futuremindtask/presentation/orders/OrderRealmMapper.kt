package com.adamk.futuremindtask.presentation.orders

import com.adamk.futuremindtask.common.arch.FutureMapper
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem
import com.adamk.futuremindtask.repository.database.OrderRealm
import io.reactivex.Single

object OrderRealmMapper : FutureMapper<List<OrderRealm>, List<OrderItem>>() {

    override fun map(orderRealm: List<OrderRealm>): Single<List<OrderItem>> = with(mutableListOf<OrderItem>()){

        orderRealm.forEach {
            this += OrderItem(
                title = it.title,
                description = it.description,
                orderId = it.orderId,
                modificationDate = it.modificationDate,
                imageUrl = it.imageUrl,
                websiteUrl = it.websiteUrl
            )
        }

        return Single.just(this.toList())
    }
}