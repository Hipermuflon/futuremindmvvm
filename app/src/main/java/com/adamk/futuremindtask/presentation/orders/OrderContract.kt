package com.adamk.futuremindtask.presentation.orders

import android.arch.lifecycle.LiveData
import com.adamk.futuremindtask.common.arch.RefreshableViewModel
import com.adamk.futuremindtask.common.ui.views.ErrorView
import com.adamk.futuremindtask.common.ui.views.RefreshableView
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem

class OrderContract {

    interface View : RefreshableView, ErrorView {
        fun displayOrders(orders: List<OrderItem>?)
        fun showWebSite(websiteUrl: String)
    }

    interface OrderViewModel : RefreshableViewModel {
        fun downloadOrders()
        val orders : LiveData<List<OrderItem>>
    }
}
