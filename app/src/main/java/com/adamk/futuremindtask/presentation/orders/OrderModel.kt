package com.adamk.futuremindtask.presentation.orders

import com.adamk.futuremindtask.common.arch.FutureModel
import com.adamk.futuremindtask.domain.services.DataService
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem
import com.adamk.futuremindtask.repository.database.OrderDao

open class OrderModel(
    private val dataService: DataService,
    private val orderDao: OrderDao
) : FutureModel() {

    open fun downloadOrders() =
        dataService.getData().async()

    open fun readOrders() =
        orderDao.getData().async()

    open fun saveOrders(orderItems : List<OrderItem>) =
        orderDao.saveData(orderItems)
}