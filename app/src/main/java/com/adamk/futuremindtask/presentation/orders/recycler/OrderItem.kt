package com.adamk.futuremindtask.presentation.orders.recycler

data class OrderItem(
    val description: String,
    val imageUrl: String,
    val websiteUrl: String,
    val modificationDate: String,
    val orderId: Int,
    val title: String
)