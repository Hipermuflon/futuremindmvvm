package com.adamk.futuremindtask.presentation.orders

import android.arch.lifecycle.MutableLiveData
import com.adamk.futuremindtask.common.arch.ErrorViewModelImpl
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem

open class OrderViewModel(
    override val model : OrderModel
) : ErrorViewModelImpl(model), OrderContract.OrderViewModel{

    override val isRefreshing = MutableLiveData<Boolean>()
    override val orders = MutableLiveData<List<OrderItem>>()

    override fun onBind() = downloadOrders()

    override fun downloadOrders() {
        isRefreshing.postValue(true)
        disposable.add(
            model.downloadOrders()
                .flatMap(ApiModelMapper::map)
                .doOnSuccess(model::saveOrders)
                .subscribe(::postOrders)
                { error ->
                    postError(error.toAppError())
                    readOrders()
                }
        )
    }

    private fun postOrders(orderList : List<OrderItem>) {
        orders.postValue(orderList)
        isRefreshing.postValue(false)
    }

    private fun readOrders() = disposable.add(
        model.readOrders()
        .flatMap(OrderRealmMapper::map)
        .subscribe(::postOrders)
        { error -> postError(error.toAppError())}
    )
}
