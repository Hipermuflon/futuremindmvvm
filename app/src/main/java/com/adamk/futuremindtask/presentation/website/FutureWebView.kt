package com.adamk.futuremindtask.presentation.website

import android.content.Context
import android.util.AttributeSet
import android.webkit.WebSettings
import android.webkit.WebView

class FutureWebView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : WebView(context, attrs, defStyleAttr) {

    init{
        settings.run {
            javaScriptEnabled = true
            allowFileAccess = true
            setAppCacheEnabled(true)
            cacheMode = WebSettings.LOAD_DEFAULT
            javaScriptCanOpenWindowsAutomatically = true
            allowContentAccess = true
            blockNetworkImage = false
            pluginState = WebSettings.PluginState.ON
            domStorageEnabled = true
        }
    }

    fun loadUrl(webData : WebData){
        webViewClient = webData.webViewClient
        loadUrl(webData.url)
    }
}
