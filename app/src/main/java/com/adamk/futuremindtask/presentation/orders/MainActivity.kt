package com.adamk.futuremindtask.presentation.orders

import com.adamk.futuremindtask.common.ui.FutureActivity
import com.adamk.futuremindtask.domain.model.HttpError
import com.adamk.futuremindtask.presentation.orders.recycler.OrderAdapter
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem
import com.adamk.futuremindtask.R
import com.adamk.futuremindtask.common.utils.ConnectionManager
import com.adamk.futuremindtask.common.utils.SnackbarFactory
import com.adamk.futuremindtask.common.ui.recycler.HorizontalDivider
import com.adamk.futuremindtask.domain.model.isRetryable
import com.adamk.futuremindtask.presentation.website.WebsiteFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.swipe_refresh.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : FutureActivity(), OrderContract.View {

    override val layoutResId = R.layout.activity_main
    override val viewModel : OrderViewModel by viewModel()
    private val isTablet:Boolean by lazy{resources.getBoolean(R.bool.isTablet)}

    override fun onViewsBound() {
        orderList.apply {
            addItemDecoration(HorizontalDivider(context))
            adapter = OrderAdapter()
        }
        swipeRefresh.setOnRefreshListener(viewModel::downloadOrders)
        viewModel.orders.observe(::getLifecycle, ::displayOrders)
        viewModel.isRefreshing.observe(::getLifecycle, ::setRefreshing)
        viewModel.error.observe(::getLifecycle, ::displayError)
    }

    override fun displayOrders(orders: List<OrderItem>?){
        orders?.let{orderList.adapter = OrderAdapter(it, ::showWebSite)}
    }

    override fun setRefreshing(isRefreshing: Boolean?) {
        swipeRefresh.isRefreshing = isRefreshing ?: false
    }

    override fun showWebSite(websiteUrl: String) {
        if(ConnectionManager.isConnectedToInternet(this).not()){
            SnackbarFactory.showErrorWithAction(swipeRefresh, HttpError.CONNECTION_ERROR) {showWebSite(websiteUrl)}
            return
        }

        if(isTablet)
            slidingPane.closePane()
        else
            flipper.displayedChild = 1

        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.websiteContent,
                WebsiteFragment.newInstance(websiteUrl)
            )
            .commit()
    }

    override fun displayError(error: HttpError?) {
        error?.let {
            if (it.isRetryable())
                SnackbarFactory.showErrorWithAction(swipeRefresh, it, viewModel::downloadOrders)
            else
                SnackbarFactory.showError(swipeRefresh, it)
        }
    }

    override fun onBackPressed() {
        if(isTablet || flipper.goBack().not())
            super.onBackPressed()
    }
}
