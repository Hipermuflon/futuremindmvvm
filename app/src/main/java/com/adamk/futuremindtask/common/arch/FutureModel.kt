package com.adamk.futuremindtask.common.arch

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class FutureModel {

    fun <T> Single<T>.async() = this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}
