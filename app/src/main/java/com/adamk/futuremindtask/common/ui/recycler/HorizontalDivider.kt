package com.adamk.futuremindtask.common.ui.recycler

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import com.adamk.futuremindtask.R
import android.support.v7.widget.RecyclerView

class HorizontalDivider (ctx: Context) : ListDivider(ctx, R.drawable.list_divider) {

    override fun adjustOutRect(outRect: Rect) {
        outRect.top = divider.intrinsicHeight
    }

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val dividerLeft = parent.paddingLeft
        val dividerRight = parent.width - parent.paddingRight

        val childCount = parent.childCount
        for (i in 0 until childCount - 1) {
            val child = parent.getChildAt(i)

            val params = child.layoutParams as RecyclerView.LayoutParams

            val dividerTop = child.bottom + params.bottomMargin
            val dividerBottom = dividerTop + divider.intrinsicHeight

            divider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
            divider.draw(canvas)
        }
    }
}