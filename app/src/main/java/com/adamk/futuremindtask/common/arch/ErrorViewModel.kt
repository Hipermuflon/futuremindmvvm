package com.adamk.futuremindtask.common.arch

import android.arch.lifecycle.LiveData
import com.adamk.futuremindtask.domain.model.HttpError

interface ErrorViewModel {
    val error : LiveData<HttpError>
}
