package com.adamk.futuremindtask.common.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.adamk.futuremindtask.common.arch.FutureViewModel

abstract class FutureActivity : AppCompatActivity() {

    abstract val layoutResId: Int
    abstract val viewModel : FutureViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
        viewModel.onBind()
        onViewsBound()
    }

    abstract fun onViewsBound()
}
