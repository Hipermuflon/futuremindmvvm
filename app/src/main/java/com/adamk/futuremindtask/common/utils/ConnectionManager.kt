package com.adamk.futuremindtask.common.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

object ConnectionManager {

    fun isConnectedToInternet(ctx: Context) = (ctx.getSystemService(Context.CONNECTIVITY_SERVICE)
            as ConnectivityManager).allNetworkInfo?.filter {it.state == NetworkInfo.State.CONNECTED}
            .orEmpty().isNotEmpty()
}
