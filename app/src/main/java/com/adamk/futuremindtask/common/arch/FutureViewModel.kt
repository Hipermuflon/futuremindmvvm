package com.adamk.futuremindtask.common.arch

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class FutureViewModel(open val model: FutureModel = FutureModel()) : ViewModel() {

    val disposable = CompositeDisposable()

    abstract fun onBind()

    override fun onCleared() {
        if (disposable.isDisposed.not())
            disposable.dispose()
    }

}
