package com.adamk.futuremindtask.common.ui.views

import com.adamk.futuremindtask.domain.model.HttpError

interface ErrorView {
    fun displayError(error: HttpError?)
}