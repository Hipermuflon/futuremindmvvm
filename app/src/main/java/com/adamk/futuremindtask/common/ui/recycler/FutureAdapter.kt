package com.adamk.futuremindtask.common.ui.recycler

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adamk.futuremindtask.presentation.orders.recycler.OrderItem

abstract class FutureAdapter<ITEM>(private val items: List<ITEM>) :
    RecyclerView.Adapter<FutureAdapter.FutureViewHolder<ITEM>>() {

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: FutureViewHolder<ITEM>, position: Int) {
        if (items.isNotEmpty())
            items[position]?.let(holder::bind)
    }

    abstract class FutureViewHolder<ITEM>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: ITEM)
    }

    class EmptyViewHolder(itemView: View) : FutureViewHolder<OrderItem>(itemView) {
        override fun bind(item: OrderItem){}
    }

    fun inflate(@LayoutRes layoutResId: Int, parent: ViewGroup) =
        LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)

    fun <BINDING : ViewDataBinding> inflateBinding(@LayoutRes layoutResId: Int, parent: ViewGroup) =
        DataBindingUtil.inflate<BINDING>(
            LayoutInflater.from(parent.context),
            layoutResId,
            parent,
            false
        )
}
