package com.adamk.futuremindtask.common.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adamk.futuremindtask.common.arch.FutureViewModel

abstract class FutureFragment: Fragment() {

    lateinit var root: View
    abstract val layoutResId: Int
    abstract val viewModel: FutureViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        root = inflater.inflate(layoutResId, container, false)
        viewModel.onBind()
        onViewsBound()
        return root
    }

    abstract fun onViewsBound()
}
