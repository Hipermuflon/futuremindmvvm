package com.adamk.futuremindtask.common.utils

import com.bumptech.glide.Glide
import android.databinding.BindingAdapter
import android.widget.ImageView
import com.adamk.futuremindtask.R
import com.bumptech.glide.request.RequestOptions

object ImageBindingAdapter {

    @BindingAdapter(value = ["imageUrl"], requireAll = false)
    @JvmStatic
    fun setImageUrl(imageView: ImageView, imageUrl: String?) {
        imageUrl?.let {
            Glide.with(imageView.context)
                .load(it)
                .apply(
                    RequestOptions()
                        .error(R.drawable.ic_refresh)
                        .placeholder(R.drawable.ic_refresh)
                )
                .into(imageView)
        }
    }
}