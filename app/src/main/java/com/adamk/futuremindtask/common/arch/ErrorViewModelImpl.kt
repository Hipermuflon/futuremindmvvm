package com.adamk.futuremindtask.common.arch

import android.arch.lifecycle.MutableLiveData
import com.adamk.futuremindtask.domain.model.HttpError

abstract class ErrorViewModelImpl(model: FutureModel = FutureModel()) : FutureViewModel( model),ErrorViewModel {

    override val error = MutableLiveData<HttpError>()

    fun postError(error: HttpError){
        this.error.postValue(error)
    }

    fun Throwable?.toAppError() = (this?.localizedMessage ?: "").toAppError()

    private fun String.toAppError(): HttpError {
        return if (contains("401") || contains("403"))
            HttpError.UNAUTHORIZED_ERROR
        else if (contains("404"))
            HttpError.NOT_FOUND_ERROR
        else if (contains("Unable to resolve host"))
            HttpError.CONNECTION_ERROR
        else if (toLowerCase().contains("time out") ||
            toLowerCase().contains("timed out")
        )
            HttpError.TIMEOUT_ERROR
        else HttpError.UNKNOWN_ERROR
    }

    fun Int.toAppError(): HttpError = when(this){
        401,403 -> HttpError.UNAUTHORIZED_ERROR
        404     -> HttpError.NOT_FOUND_ERROR
        else   -> HttpError.UNKNOWN_ERROR
    }
}