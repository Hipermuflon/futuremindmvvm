package com.adamk.futuremindtask.common.arch

import android.arch.lifecycle.LiveData

interface RefreshableViewModel {
    val isRefreshing : LiveData<Boolean>
}
