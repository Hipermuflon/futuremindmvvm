package com.adamk.futuremindtask.common.utils

import android.content.Context
import android.support.design.widget.Snackbar
import android.view.View
import com.adamk.futuremindtask.R
import com.adamk.futuremindtask.domain.model.HttpError

object SnackbarFactory {

    fun showError(view: View, error: HttpError) = with(view) {
        Snackbar.make(this, getMessage(context, error), Snackbar.LENGTH_LONG).show()
    }

    fun showErrorWithAction(view: View, error: HttpError, action: () -> Unit) = with(view) {
        Snackbar.make(this, getMessage(context, error), Snackbar.LENGTH_LONG).setRetryAction(context, action).show()
    }

    private fun getMessage(ctx: Context, error: HttpError) = when (error) {
        HttpError.UNAUTHORIZED_ERROR -> ctx.getString(R.string.unauthorized_error)
        HttpError.CONNECTION_ERROR -> ctx.getString(R.string.connection_error)
        HttpError.NOT_FOUND_ERROR -> ctx.getString(R.string.not_found_error)
        HttpError.TIMEOUT_ERROR -> ctx.getString(R.string.timeout_error)
        else -> ctx.getString(R.string.unknown_error)
    }
}

private fun Snackbar.setRetryAction(ctx: Context, action: () -> Unit) = setAction(ctx.resources.getString(R.string.retry)) { action }