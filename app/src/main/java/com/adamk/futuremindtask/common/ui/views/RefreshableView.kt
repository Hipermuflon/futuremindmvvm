package com.adamk.futuremindtask.common.ui.views

interface RefreshableView {
    fun setRefreshing(isRefreshing: Boolean?)
}
