package com.adamk.futuremindtask.di

import com.adamk.futuremindtask.domain.services.DataService
import com.adamk.futuremindtask.presentation.orders.OrderModel
import com.adamk.futuremindtask.presentation.orders.OrderViewModel
import com.adamk.futuremindtask.presentation.website.WebsiteViewModel
import com.adamk.futuremindtask.repository.database.OrderDao
import com.adamk.futuremindtask.repository.network.DataServiceImpl
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

fun getModules() = listOf(orderModule,websiteModule)

private val orderModule = module {

    single<DataService> { DataServiceImpl() }
    single { OrderDao()}
    factory { OrderModel(get(), get()) }

    viewModel { OrderViewModel(get()) }
}

private val websiteModule = module {
    viewModel { WebsiteViewModel() }
}
