package com.adamk.futuremindtask.domain.services

import com.adamk.futuremindtask.domain.model.DataApiModel
import io.reactivex.Single
import retrofit2.http.GET

interface DataService {

    @GET("recruitment-task/")
    fun getData(): Single<List<DataApiModel>>
}